package com.prashant.api;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.prashant.Tickets;


public class GuardarTickets {
	
	private static Map<String, Tickets>tickets = new HashMap<String, Tickets>(); 
	
	
	/**
	 *guardando tickets por id
	 */	
	public static void guardarTickets(Tickets ticket){ 
		tickets.put(ticket.getId(), ticket);		
	}
	
	/**
	 *listando todos os tickets
	 */	
	public static Collection<Tickets> listar(){ 
		return tickets.values();
	}
	
	/**
	 * verificando se o ticket já existe	 
	 */
	public static boolean verificarTicket(Tickets ticket){
		return tickets.containsKey(ticket.getId());
	}
	
	public static Tickets retornarTicket(String tag){
		
	return tickets.get(tag);	
	}
}
