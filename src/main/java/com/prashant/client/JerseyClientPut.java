package com.prashant.client;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

public class JerseyClientPut {

	public static void main(String[] args) {

		try {
			
			ClientConfig config = new DefaultClientConfig();
			
			Client client = Client.create(config);
			
			client.addFilter(new HTTPBasicAuthFilter(
					"beneficiofacil@zipmail.com.br", "facil1234"));	

			WebResource webResource = client
					.resource("https://developmentbrazil.zendesk.com/api/v2/tickets/11.json");

			String input = "{\"ticket\":{\"tags\":\"adicional\"}}"; 

			ClientResponse response = webResource.type("application/json")
					.put(ClientResponse.class, input);
			
			System.out.println(response);

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}

			System.out.println("Output from Server .... \n");
			String output = response.getEntity(String.class);
			System.out.println(output);

		} catch (Exception e) {

			e.printStackTrace();

		}

	}

}
