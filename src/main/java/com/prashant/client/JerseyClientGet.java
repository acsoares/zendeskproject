package com.prashant.client;

import java.util.InputMismatchException;
import java.util.Scanner;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.prashant.Tickets;
import com.prashant.api.GuardarTickets;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

public class JerseyClientGet {

	String output;

	public String capturaTag() {
		Scanner scan = new Scanner(System.in);
		System.out.println("\n\tDigite a tag:\n");
		String tag = "";
		tag = scan.next();
		System.out.println("\nResultado da busca pela tag:\t\"" + tag + "\"");
		return tag;
	}

	public int retornaOpcao() {
		int opcao = 0;
		do {
			try {
				Scanner scan = new Scanner(System.in);
				System.out.println("****************************************");
				System.out.println("\tDigite a opção desejada:");
				System.out.println("****************************************\n");
				System.out.println("(1) - para retornar todos os tickets:");
				System.out.println("(2) - para pesquisar por tag:");
				// System.out.println("(3) - para pesquisar tickets sem tags:\n");
				System.out.println("****************************************");

				opcao = scan.nextInt();
			}

			catch (InputMismatchException erro) {

				System.out.println("Introduza apenas números inteiros!");
			}

		} while (opcao != 1 && opcao != 2);

		return opcao;

	}

	public String passandoURL() {
		String URL = "";

		switch (retornaOpcao()) {

		case 1:
			System.out.println("Retornando todos os tickets ...");
			URL = "https://developmentbrazil.zendesk.com/api/v2/tickets.json"; // ?page=1&per_page=2
			break;
		case 2:
			URL = "https://developmentbrazil.zendesk.com/api/v2/search.json?query=tags:"+ capturaTag();
			break;
		// Não foi possível retornar tickets sem tag, o zendesk não aceita essa
		// requisição
		// case 3:
		// System.out.println("Retornando os tickets sem tag ... ");
		// URL =
		// "https://developmentbrazil.zendesk.com/api/v2/search.json?query=tags:";
		// break;
		}

		return URL;
	}
	
	public Client retornaCliente(){//OK
		ClientConfig config = new DefaultClientConfig();

		Client client = Client.create(config);

		client.addFilter(new HTTPBasicAuthFilter(
				"beneficiofacil@zipmail.com.br", "facil1234"));
		System.out.println(client);
		return client;	
		
	}

	public void logar() {
		try {			

			WebResource webResource = retornaCliente().resource(passandoURL());

			ClientResponse response = webResource.accept("application/json")
					.get(ClientResponse.class);

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}

			output = response.getEntity(String.class);
			output = output.replaceAll("\"results\"", "\"tickets\"");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void criarJson() throws JSONException {

		JSONObject tickets = new JSONObject(output);

		JSONArray arrTickets = tickets.getJSONArray("tickets");
		if (arrTickets.length() == 0) {
			System.out.println("\nTag inexistente!");
		}
		for (int i = 0; i < arrTickets.length(); i++) {
			Tickets ticket = new Tickets();

			/**
			 * recupera tickets de índice "i" no array
			 * */
			JSONObject f = arrTickets.getJSONObject(i);

			ticket.setUrl(f.getString("url"));
			ticket.setId(f.getString("id"));
			ticket.setExternal_id(f.getString("external_id"));
			ticket.setVia(f.getString("via"));
			ticket.setCreated_at(f.getString("created_at"));
			ticket.setUpdated_at(f.getString("updated_at"));
			ticket.setType(f.getString("type"));
			ticket.setSubject(f.getString("subject"));
			ticket.setDescription(f.getString("description"));
			ticket.setPriority(f.getString("priority"));
			ticket.setStatus(f.getString("status"));
			ticket.setRecipient(f.getString("recipient"));
			ticket.setRequester_id(f.getString("requester_id"));
			ticket.setSubmitter_id(f.getString("submitter_id"));
			ticket.setAssignee_id(f.getString("assignee_id"));
			ticket.setOrganization_id(f.getString("organization_id"));
			ticket.setGroup_id(f.getString("group_id"));
			ticket.setCollaborator_ids(f.getString("collaborator_ids")); // array
			ticket.setForum_topic_id(f.getString("forum_topic_id"));
			ticket.setProblem_id(f.getString("problem_id"));
			ticket.setHas_incidents(Boolean.valueOf(f.getString("has_incidents")));
			ticket.setDue_at(f.getString("due_at"));
			
			/**Um array pode ter várias tags*/
			JSONArray arrTags = f.getJSONArray("tags");

			for (int k = 0; k < arrTags.length(); k++) {
				ticket.guardarTags(arrTags.getString(k));
			}

			ticket.setFields(f.getString("fields"));
			ticket.setCustom_fields(f.getString("custom_fields"));
			ticket.setSatisfaction_rating(f.getString("satisfaction_rating"));
			ticket.setSharing_agreement_ids(f.getString("sharing_agreement_ids"));
			
			GuardarTickets.guardarTickets(ticket);
		}		

	}	

	public void exibirJson() {
		StringBuffer buffer = new StringBuffer();

		for (Tickets tks : GuardarTickets.listar()) {
			buffer.append("\nId:                   \t" + tks.getId());
			buffer.append("\nExternal ID:          \t" + tks.getExternal_id());
			buffer.append("\nVia:                  \t" + tks.getVia());
			buffer.append("\nCreated At:           \t" + tks.getCreated_at());
			buffer.append("\nUpdated At:           \t" + tks.getUpdated_at());
			buffer.append("\nType:                 \t" + tks.getType());
			buffer.append("\nSubject:              \t" + tks.getSubject());
			buffer.append("\nDescription:          \t" + tks.getDescription());
			buffer.append("\nPriority:             \t" + tks.getPriority());
			buffer.append("\nStatus:               \t" + tks.getStatus());
			buffer.append("\nRecipient:            \t" + tks.getRecipient());
			buffer.append("\nSubmitter_id:         \t" + tks.getSubmitter_id());
			buffer.append("\nAssignee_id:          \t" + tks.getAssignee_id());
			buffer.append("\nRequester_id:         \t" + tks.getRequester_id());
			buffer.append("\nOrganization_id:      \t" + tks.getOrganization_id());
			buffer.append("\nGroup_id:             \t" + tks.getGroup_id());
			buffer.append("\nCollaborator_ids:     \t" + tks.getCollaborator_ids());
			buffer.append("\nForum_topic_id:       \t" + tks.getForum_topic_id());
			buffer.append("\nProblem_id:           \t" + tks.getProblem_id());
			buffer.append("\nHas_incidents:        \t" + tks.isHas_incidents());
			buffer.append("\nDue_at:               \t" + tks.getDue_at());
			buffer.append("\nTags:                 \t" + tks.listarTags());
			buffer.append("\nFields:               \t" + tks.getFields());
			buffer.append("\nCustom_fields:        \t" + tks.getCustom_fields());
			buffer.append("\nSatisfaction_rating:  \t" + tks.getSatisfaction_rating());
			buffer.append("\nSharing_agreement_ids:\t" + tks.getSharing_agreement_ids() + "\n");
			buffer.append("\n************************************************************\n");
		}

		System.out.println(buffer);
	}	

	public static void main(String[] args) throws JSONException {
		Scanner sc = new Scanner(System.in);
		String sair = "n";

		JerseyClientGet jcl = new JerseyClientGet();

		while ((sair.equals("n")) || (sair.equals("N"))) {
		
			jcl.logar();
			jcl.criarJson();
			jcl.exibirJson();
			
			
			System.out.println("Deseja sair? S/N");
			sair = sc.next();
		}

		/**
		 * 
		 * 
		 * JSONObject obj = new JSONObject(output);
		 * 
		 * List<String> list = new ArrayList<String>(); 
		 * JSONArray array = obj.getJSONArray("tickets");
		 * for(int i = 0 ; i < array.length() ; i++){
		 * 	list.add(array.getJSONObject(i).getString("tags"));
		 *  }
		 * 
		 * System.out.println(list);
		 * 
		 * ObjectMapper mapper = new ObjectMapper(); Tickets ticket =
		 * mapper.readValue(output, Tickets.class);
		 * 
		 * System.out.println(ticket);
		 **/
	}
}