package com.prashant;

import java.util.ArrayList;
import java.util.List;

public class Tickets {

	private String url;

	private String id;

	private String external_id;

	private String via;

	private String created_at;

	private String updated_at;

	private String type;

	private String subject;

	private String description;

	private String priority;

	private String status;

	private String recipient;

	private String requester_id;

	private String submitter_id;

	private String assignee_id;

	private String organization_id;

	private String group_id;

	private String collaborator_ids; // array

	private String forum_topic_id;

	private String problem_id;

	private Boolean has_incidents;

	private String due_at;

	private List<String> tags = new ArrayList<String>();

	private String fields; // array

	private String custom_fields; // array

	private String satisfaction_rating;

	private String sharing_agreement_ids; // array

	// private String next_page;
	//
	// private String previous_page;

	private String count;

	private String user;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getExternal_id() {
		return external_id;
	}

	public void setExternal_id(String external_id) {
		this.external_id = external_id;
	}

	public String getVia() {
		return via;
	}

	public void setVia(String via) {
		this.via = via;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getType() {

		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getRequester_id() {
		return requester_id;
	}

	public void setRequester_id(String requester_id) {
		this.requester_id = requester_id;
	}

	public String getSubmitter_id() {
		return submitter_id;
	}

	public void setSubmitter_id(String submitter_id) {
		this.submitter_id = submitter_id;
	}

	public String getAssignee_id() {
		return assignee_id;
	}

	public void setAssignee_id(String assignee_id) {
		this.assignee_id = assignee_id;
	}

	public String getOrganization_id() {
		return organization_id;
	}

	public void setOrganization_id(String organization_id) {
		this.organization_id = organization_id;
	}

	public String getGroup_id() {
		return group_id;
	}

	public void setGroup_id(String group_id) {
		this.group_id = group_id;
	}

	public String getCollaborator_ids() {
		return collaborator_ids;
	}

	public void setCollaborator_ids(String collaborator_ids) {
		this.collaborator_ids = collaborator_ids;
	}

	public String getForum_topic_id() {
		return forum_topic_id;
	}

	public void setForum_topic_id(String forum_topic_id) {
		this.forum_topic_id = forum_topic_id;
	}

	public String getProblem_id() {
		return problem_id;
	}

	public void setProblem_id(String problem_id) {
		this.problem_id = problem_id;
	}

	public Boolean isHas_incidents() {
		return has_incidents;
	}

	public void setHas_incidents(Boolean has_incidents) {
		this.has_incidents = has_incidents;
	}

	public String getDue_at() {
		return due_at;
	}

	public void setDue_at(String due_at) {
		this.due_at = due_at;
	}

	public void guardarTags(String tags) {
		this.tags.add(tags);
	}

	public List<String> listarTags() {
		return tags;
	}

	public String getFields() {
		return fields;
	}

	public void setFields(String fields) {
		this.fields = fields;
	}

	public String getCustom_fields() {
		return custom_fields;
	}

	public void setCustom_fields(String custom_fields) {
		this.custom_fields = custom_fields;
	}

	public String getSatisfaction_rating() {
		return satisfaction_rating;
	}

	public void setSatisfaction_rating(String satisfaction_rating) {
		this.satisfaction_rating = satisfaction_rating;
	}

	public String getSharing_agreement_ids() {
		return sharing_agreement_ids;
	}

	public void setSharing_agreement_ids(String sharing_agreement_ids) {
		this.sharing_agreement_ids = sharing_agreement_ids;
	}

	// public String getNext_page() {
	// return next_page;
	// }
	//
	// public void setNext_page(String next_page) {
	// this.next_page = next_page;
	// }
	//
	// public String getPrevious_page() {
	// return previous_page;
	// }
	//
	// public void setPrevious_page(String previous_page) {
	// this.previous_page = previous_page;
	// }

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

//	public boolean containsAnyTag(String[] tags) {
//		for (String s : tags) {
//			if (this.listarTags().contains(s)) {
//				return true;
//			}
//		}
//		return false;
//	}

	/*-------Alt+Shift+S-----*/

}
