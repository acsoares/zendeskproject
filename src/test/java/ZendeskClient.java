import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;


public class ZendeskClient {
	
	
	
	public Client setAutorizacao(String usuario, String senha) {
		ClientConfig config = new DefaultClientConfig();

		Client client = Client.create(config);

		client.addFilter(new HTTPBasicAuthFilter(usuario, senha));
		return client;	
		
	}

	public TicketQuery consultarTickets() {
		// TODO Auto-generated method stub
		
		TicketQuery ticketQuery = new TicketQuery();
		String output;
		
		try {			
			
			String usuario = "beneficiofacil@zipmail.com.br";
			String senha = "facil1234";
			WebResource webResource = setAutorizacao(usuario, senha).resource("https://developmentbrazil.zendesk.com/api/v2/tickets.json");

			ClientResponse response = webResource.accept("application/json")
					.get(ClientResponse.class);
			
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}

			output = response.getEntity(String.class);
			output = output.replaceAll("\"results\"", "\"tickets\"");
			System.out.println(output);

		} catch (Exception e) {
			e.printStackTrace();
		}
	return ticketQuery;
	
	}

}
