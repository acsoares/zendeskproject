import java.util.ArrayList;
import java.util.List;


public class Ticket {

	private String id;
	
	private List<String> tags = new ArrayList<String>();
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public void guardarTags(String tags) {
		this.tags.add(tags);
	}

	public List<String> listarTags() {
		return tags;
	}
	
}
